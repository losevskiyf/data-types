package data_types;

public class DataTypesApp {

    private static int getNumber(int recordBookNumber){
        //Создаем копию номера зачетной книжки
        int temp = recordBookNumber;
        //Определяем номер студента в журнале группы
        temp = temp % 100;
        //Уменьшаем промежуточный результат на единицу
        temp--;
        //Находим остаток от деления на 26
        temp = temp%26;
        //Увеличиваем промежуточный результат на единицу
        temp++;
        return temp;
    }

    private static Character getCharacterAccordingToNumber(int characterCode){
        //Первый символ латиницы 'A' в UTF-16 соответствует номеру 65
        characterCode = characterCode + 64;
        if(characterCode > 126){
            return null;
        }else {
            return (char)characterCode;
        }
    }

    public static void main(String[] args) {

        Types types = new Types();
        System.out.println(types);

        int numberOfStudent = getNumber(types.getRecordBookNumber());
        System.out.println("Увеличенное на единицу значение остатка от деления на 26 " +
                "уменьшенного на единицу номера студента в журнале группы = " + numberOfStudent);

        Character CharacterAccordingToNumber = getCharacterAccordingToNumber(numberOfStudent);
        if(CharacterAccordingToNumber==null)
            System.out.println("В функцию передано значение, выходящее за допустимый предел");
        else
            System.out.println("Символ английского алфавита в верхнем регистре, " +
                    "номер которого соответствует найденному ранее значению = "
                    + CharacterAccordingToNumber);

    }

}