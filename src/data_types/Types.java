package data_types;

public class Types{
    private final int recordBookNumber = 0xE4985;
    private final long phoneNumber = 89215986856L;
    private final byte twoLastPhoneNumberNums = 0b111000;
    private final short fourLastPhoneNumberNums = 015310;

    public int getRecordBookNumber(){
        return recordBookNumber;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public byte getTwoLastPhoneNumberNums() {
        return twoLastPhoneNumberNums;
    }

    public short getFourLastPhoneNumberNums() {
        return fourLastPhoneNumberNums;
    }

    @Override
    public String toString() {
        return  "-------------------------------------------\n" +
                "recordBookNumber = " + recordBookNumber + "\n" +
                "phoneNumber = " + phoneNumber + "\n" +
                "twoLastPhoneNumberNums = " + twoLastPhoneNumberNums + "\n" +
                "fourLastPhoneNumberNums = " + fourLastPhoneNumberNums +
                "\n-------------------------------------------";
    }
}